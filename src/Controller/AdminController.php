<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Task;
use App\Entity\TimeTracker;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AdminController extends EasyAdminController
{

    /**
     * @Route("/", name="easyadmin")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function indexAction(Request $request)
    {
        dump($this->getUser());
        return parent::indexAction($request);
    }

    protected function persistEntity($entity)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (method_exists($entity, 'setCreateBy')) {
            $entity->setCreateBy($user->getId());
        }
        parent::persistEntity($entity);
    }

    /**
     * @param object $entity
     * @throws \Exception
     */
    protected function updateEntity($entity)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if (method_exists($entity, 'setUpdateAt')) {
            $entity->setUpdateAt(new \DateTime());
        }
        if (method_exists($entity, 'setCreateBy')){
            $entity->setCreateBy($user->getId());
        }
        parent::updateEntity($entity);
    }

    /**
     * @return Response
     */
    protected function listAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);
        $user = $this->get('security.token_storage')->getToken()->getUser();
       if($user && !is_string($user)){
        if( in_array('ROLE_ADMIN', $user->getRoles()) ) {
            $this->entity['list']['dql_filter'];
        }else{
//            if($this->entity instanceof Project::class && Task::class && TimeTracker::class){
            $this->entity['list']['dql_filter'] = "entity.createBy = " .$user->getId();
//            }
        }}
       else{
           dump($user);
           die;
       }
        return parent::listAction();
    }


}
