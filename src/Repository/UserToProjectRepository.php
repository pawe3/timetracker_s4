<?php

namespace App\Repository;

use App\Entity\UserToProject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserToProject|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserToProject|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserToProject[]    findAll()
 * @method UserToProject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserToProjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserToProject::class);
    }

    // /**
    //  * @return UserToProject[] Returns an array of UserToProject objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserToProject
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
