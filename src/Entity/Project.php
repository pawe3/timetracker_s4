<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $projectName;

    /**
     * @var \DateTime $createAt
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @var \DateTime $updateAt
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="project", cascade={"persist", "remove"}))
     */
    private $tasks;



    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserToProject", mappedBy="project")
     */
    private $userToProjects;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $createBy;

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
        $this->userToProjects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProjectName(): ?string
    {
        return $this->projectName;
    }

    public function setProjectName(string $projectName): self
    {
        $this->projectName = $projectName;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }


    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setProject($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
            // set the owning side to null (unless already changed)
            if ($task->getProject() === $this) {
                $task->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function __toString() {
        return $this->getProjectName();
    }

    /**
     * @return Collection|UserToProject[]
     */
    public function getUserToProjects(): Collection
    {
        return $this->userToProjects;
    }

    public function addUserToProject(UserToProject $userToProject): self
    {
        if (!$this->userToProjects->contains($userToProject)) {
            $this->userToProjects[] = $userToProject;
            $userToProject->setProject($this);
        }

        return $this;
    }

    public function removeUserToProject(UserToProject $userToProject): self
    {
        if ($this->userToProjects->contains($userToProject)) {
            $this->userToProjects->removeElement($userToProject);
            // set the owning side to null (unless already changed)
            if ($userToProject->getProject() === $this) {
                $userToProject->setProject(null);
            }
        }

        return $this;
    }

    public function getCreateBy(): ?string
    {
        return $this->createBy;
    }

    public function setCreateBy(?string $createBy): self
    {
        $this->createBy = $createBy;

        return $this;
    }
}
