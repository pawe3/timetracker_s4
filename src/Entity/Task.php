<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $taskName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $descriptionTask;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="tasks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TimeTracker", mappedBy="task")
     */
    private $timeTrakers;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $createBy;

    public function __construct()
    {
        $this->timeTrakers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTaskName(): ?string
    {
        return $this->taskName;
    }

    public function setTaskName(string $taskName): self
    {
        $this->taskName = $taskName;

        return $this;
    }

    public function getDescriptionTask(): ?string
    {
        return $this->descriptionTask;
    }

    public function setDescriptionTask(?string $descriptionTask): self
    {
        $this->descriptionTask = $descriptionTask;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return Collection|TimeTracker[]
     */
    public function getTimeTrakers(): Collection
    {
        return $this->timeTrakers;
    }

    public function addTimeTraker(TimeTracker $timeTraker): self
    {
        if (!$this->timeTrakers->contains($timeTraker)) {
            $this->timeTrakers[] = $timeTraker;
            $timeTraker->setTask($this);
        }

        return $this;
    }

    public function removeTimeTraker(TimeTracker $timeTraker): self
    {
        if ($this->timeTrakers->contains($timeTraker)) {
            $this->timeTrakers->removeElement($timeTraker);
            // set the owning side to null (unless already changed)
            if ($timeTraker->getTask() === $this) {
                $timeTraker->setTask(null);
            }
        }

        return $this;
    }
    /**
     * @return string|null
     */
    public function __toString() {
        return $this->getTaskName();
    }

    public function getCreateBy(): ?string
    {
        return $this->createBy;
    }

    public function setCreateBy(?string $createBy): self
    {
        $this->createBy = $createBy;

        return $this;
    }

}
