<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserToProjectRepository")
 */
class UserToProject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rola;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="userToProjects")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id",
     * nullable=false, onDelete="CASCADE")
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userToProjects")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",
     * nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->project = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getRola(): ?string
    {
        return $this->rola;
    }

    public function setRola(string $rola): self
    {
        $this->rola = $rola;

        return $this;
    }

    /**
     * @return User|ArrayCollection|null
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * @return Project|ArrayCollection|null
     */

    public function getProject()
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function __toString()
    {
        return $this->getRola();
    }


}
